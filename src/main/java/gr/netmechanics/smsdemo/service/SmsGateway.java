package gr.netmechanics.smsdemo.service;

import static gr.netmechanics.smsdemo.SmsConstants.APIFONE_API_URL;
import static gr.netmechanics.smsdemo.SmsConstants.balanceEndpoint;
import static gr.netmechanics.smsdemo.SmsConstants.date_format;
import static gr.netmechanics.smsdemo.SmsConstants.header_ApifonWS_Date;
import static gr.netmechanics.smsdemo.SmsConstants.header_ApplicationJson;
import static gr.netmechanics.smsdemo.SmsConstants.header_Authorization;
import static gr.netmechanics.smsdemo.SmsConstants.header_Authorization_Value;
import static gr.netmechanics.smsdemo.SmsConstants.header_Charset_Name;
import static gr.netmechanics.smsdemo.SmsConstants.header_ContentType;
import static gr.netmechanics.smsdemo.SmsConstants.http_POST;
import static gr.netmechanics.smsdemo.SmsConstants.secretKey;
import static gr.netmechanics.smsdemo.SmsConstants.sendSmsEndpoint;
import static gr.netmechanics.smsdemo.SmsConstants.timezone_GMT;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import javax.inject.Inject;

import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.view.Results;
import com.google.common.io.ByteSource;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import gr.netmechanics.smsdemo.model.SmsRequest;
import gr.netmechanics.smsdemo.responses.Balance;
import org.apache.commons.codec.Charsets;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

/**
 * @author Nikos Papadakis
 */
public class SmsGateway {

    @Inject
    private Result result;

    private HttpResponse adjustRequest(String body, String endpoint) throws URISyntaxException, IOException  {
        StringEntity input = new StringEntity(body, header_Charset_Name);
        input.setContentType(header_ApplicationJson);

        HttpPost request = new HttpPost();
        request.setEntity(input);
        request.setURI(new URI(APIFONE_API_URL + endpoint));

        SimpleDateFormat dateFormat = new SimpleDateFormat(date_format, Locale.US);
        dateFormat.setTimeZone(TimeZone.getTimeZone(timezone_GMT));
        String strDate = dateFormat.format(new Date());

        HttpClient httpClient = HttpClientBuilder.create().build();
        URI tmUri = new URI(endpoint);

        String signature = SignatureGenerator.getSignature(secretKey, http_POST, tmUri.getPath(), body, strDate);
        request.setHeader(header_ContentType, header_ApplicationJson);
        request.setHeader(header_Authorization, header_Authorization_Value + signature);
        request.setHeader(header_ApifonWS_Date, strDate);

        return httpClient.execute(request);
    }

    /* Get balance */
    public void balance() throws URISyntaxException, IOException {
        String body = "";
        HttpResponse response = adjustRequest(body, balanceEndpoint);

        Gson gson = new Gson();
        Balance balanceResponse = gson.fromJson(responseBodyText(response), Balance.class);

        /* For test purpose we serialize and return POJO */
        result.use(Results.json()).withoutRoot().from(balanceResponse).serialize();
    }

    /* Send sms */
    public void sendSms(SmsRequest smsRequest) throws URISyntaxException, IOException {
        Gson gson = new GsonBuilder().create();
        String smsRequestStr = gson.toJson(smsRequest);
        HttpResponse response = adjustRequest(smsRequestStr, sendSmsEndpoint);

//        Gson gson2 = new Gson();
//        Sms smsResponse = gson2.fromJson(responseBodyText(response), Sms.class);

        /* For test purpose we serialize and return POJO */
        result.use(Results.json()).withoutRoot().from(response).serialize();
    }

    private String responseBodyText(HttpResponse response) throws IOException {
        InputStream inputStream = response.getEntity().getContent();
        ByteSource byteSource = new ByteSource() {
            @Override
            public InputStream openStream() throws IOException {
                return inputStream;
            }
        };

        return byteSource.asCharSource(Charsets.UTF_8).read();
    }
}
