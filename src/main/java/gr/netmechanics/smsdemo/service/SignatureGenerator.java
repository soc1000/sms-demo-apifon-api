package gr.netmechanics.smsdemo.service;

import static gr.netmechanics.smsdemo.SmsConstants.hash_hmac;
import static gr.netmechanics.smsdemo.SmsConstants.header_Charset_Name;

import java.io.UnsupportedEncodingException;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

/**
 * @author Nikos Papadakis
 */

public class SignatureGenerator {
    public static String getSignature(String secretKey, String method, String uri, String body, String strDate) {

        String toSign = method + "\n"
            + uri + "\n"
            + body + "\n"
            + strDate;

        Mac mac = null;
        try {
            byte[] secretyKeyBytes = secretKey.getBytes(header_Charset_Name);
            SecretKeySpec secretKeySpec = new SecretKeySpec(secretyKeyBytes, hash_hmac);
            mac = Mac.getInstance(hash_hmac);
            mac.init(secretKeySpec);

        } catch (Exception ex) {
            System.out.println(ex);
        }

        String signature = null;
        byte[] data;
        byte[] rawHmac;
        try {
            data = toSign.getBytes(header_Charset_Name);
            rawHmac = mac.doFinal(data);
            Base64 encoder = new Base64();
            signature = new String(encoder.encode(rawHmac));

        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("UTF-8 is unsupported!", e);
        }

        return signature;
    }
}

