package gr.netmechanics.smsdemo;

/**
 * @author Nikos Papadakis
 */
public interface SmsConstants {

    // TODO callback url cannot be tested from localhost
    String SERVER_CALLBACK_URL = "http://localhost:8080/smsdemo/smsapi/callback";

    /* 'token' and 'secretKey' were provided by Apifon upon request, for development and testing purposes */
    String token        = "c5c238db685ffe90b264a2ff50e5325b29b2a3a8b53b8b6943246f79ed032bab";
    String secretKey    = "Dv9#fi%Qdw+97g9O";

    /* Apifone API Endpoints */
    String APIFONE_API_URL = "https://ars.apifon.com";
    String balanceEndpoint = "/services/balance";
    String sendSmsEndpoint = "/services/sms/send";

    /* Request Headers*/
    String header_ContentType       = "Content-Type";
    String header_ApplicationJson   = "application/json";
    String header_Authorization     = "Authorization";
    String header_ApifonWS_Date     = "X-ApifonWS-Date";
    String header_Charset_Name      = "UTF-8";
    String header_Authorization_Value = "ApifonWS " + token + ":";

    String date_format  = "EEE, dd MMM yyyy HH:mm:ss z";
    String hash_hmac    = "HmacSHA256";
    String timezone_GMT = "GMT";

    /* HTTP methods */
    String http_POST = "POST";
    String http_GET  = "GET";
}
