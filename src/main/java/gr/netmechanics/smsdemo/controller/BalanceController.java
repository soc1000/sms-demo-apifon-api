package gr.netmechanics.smsdemo.controller;

import java.io.IOException;
import java.net.URISyntaxException;
import javax.inject.Inject;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import gr.netmechanics.smsdemo.service.SmsGateway;

/**
 * @author Nikos Papadakis
 */

@Controller
@Path("/smsapi")
public class BalanceController {

    @Inject
    private SmsGateway smsGateway;

    @Get("/balance")
    public void balance() throws URISyntaxException, IOException {
        try {
            smsGateway.balance();

        } catch (URISyntaxException | IOException d) {
            System.out.println("error occured while requesting balance");
        }
    }
}
