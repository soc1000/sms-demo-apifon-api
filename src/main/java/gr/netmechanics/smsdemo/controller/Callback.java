package gr.netmechanics.smsdemo.controller;

import javax.inject.Inject;

import br.com.caelum.vraptor.Consumes;
import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.serialization.gson.WithoutRoot;
import gr.netmechanics.smsdemo.responses.CallbackResponse;

/**
 * @author Nikos Papadakis
 */
@Controller
@Path("smsapi")
public class Callback {

    @Inject
    private Result result;

    @Consumes(value="application/json", options = WithoutRoot.class)
    @Post("/callback")
    public void callback(CallbackResponse callbackResponse) {

        System.out.println("callback response = " + callbackResponse.toString());
//        Gson gson = new Gson();
//        String response = gson.toJson(callbackResponse, CallbackResponse.class);
//
//        /* For test purpose we serialize and return POJO */
//        result.use(Results.json()).withoutRoot().from(response).serialize();
    }
}
