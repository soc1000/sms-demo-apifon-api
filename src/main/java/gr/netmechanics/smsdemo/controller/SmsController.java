package gr.netmechanics.smsdemo.controller;

import static gr.netmechanics.smsdemo.SmsConstants.SERVER_CALLBACK_URL;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import gr.netmechanics.smsdemo.model.Message;
import gr.netmechanics.smsdemo.model.SmsRequest;
import gr.netmechanics.smsdemo.model.Subscribers;
import gr.netmechanics.smsdemo.service.SmsGateway;

/**
 * @author Nikos Papadakis
 */

@Controller
@Path("/smsapi")
public class SmsController {

    @Inject
    private SmsGateway smsGateway;

    @Inject
    private Message message;

    @Inject
    private Subscribers subscribers;

    @Inject
    private SmsRequest smsRequest;

    // Demonstrates 'send' action, with a simple text message and one recipient
    @Get("/sendsms")
    public void sendsms() throws URISyntaxException, IOException {
        try {
            message.setSender_id("TestCompany");
            message.setText("Your booking ref is 123456 and cost is 241 Euros");
            subscribers.setNumber("306947867300"); // MSISDN with country code

            smsRequest.setMessage(message);
            List<Subscribers> subscribersList = new ArrayList<>();
            subscribersList.add(subscribers);
            smsRequest.setSubscribers(subscribersList);

            smsRequest.setCallback_url(SERVER_CALLBACK_URL);

            smsGateway.sendSms(smsRequest);

        } catch (URISyntaxException | IOException d) {
            System.out.println("error occured while sending sms");
        }
    }
}
