package gr.netmechanics.smsdemo.model;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Nikos Papadakis
 */
public class Message {

    @Getter @Setter
    private String text;

    //If numeric, minimum length is 1, maximum length is 16 digits. If alphanumeric, minimum length is 1, maximum length is 11 characters.
    @Getter @Setter
    private String sender_id;

}
