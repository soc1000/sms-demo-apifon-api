package gr.netmechanics.smsdemo.model;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Nikos Papadakis
 */
public class Subscribers {

    // Mobile number (MSISDN) to deliver the message to. Number is in international format and is only digits between 7-15 digits long. First digit cannot be a 0.
    @Getter @Setter
    private String number;

    @Getter @Setter
    private String custom_id;

    @Getter @Setter
    private String params;
}
