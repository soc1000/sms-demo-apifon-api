package gr.netmechanics.smsdemo.model;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Nikos Papadakis
 */
public class SmsRequest {

    @Getter @Setter
    private List<Subscribers> subscribers = new ArrayList<>();

    @Getter @Setter
    private Message message;

    /* Below variables are optional */
    @Getter @Setter
    private String reference_id;

    @Getter @Setter
    private String callback_url;

    @Getter @Setter
    private String tte;

    @Getter @Setter
    private String date;
}
