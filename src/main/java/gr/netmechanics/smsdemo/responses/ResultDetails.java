package gr.netmechanics.smsdemo.responses;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Nikos Papadakis
 */
public class ResultDetails {

    @Setter @Getter
    private String message_id;

    @Setter @Getter
    private String custom_id;

    @Setter @Getter
    private int length;

    @Setter @Getter
    private String short_url;

    @Setter @Getter
    private String short_code;

}
