package gr.netmechanics.smsdemo.responses;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Nikos Papadakis
 */
public class Subscriptions {

    @Setter @Getter
    private String senderId;

    @Setter @Getter
    private String available;

    @Setter @Getter
    private String total;

    @Setter @Getter
    private String endDate;

}
