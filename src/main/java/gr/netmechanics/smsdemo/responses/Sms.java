package gr.netmechanics.smsdemo.responses;

import java.util.HashMap;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Nikos Papadakis
 */
public class Sms {

    @Setter @Getter
    private String request_id;

    @Setter @Getter
    private String reference;

    @Setter @Getter
    private Map<String, ResultDetails> results = new HashMap<>();

    @Setter @Getter
    private ResultInfo resultInfo;
}
