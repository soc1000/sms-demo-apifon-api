package gr.netmechanics.smsdemo.responses;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Nikos Papadakis
 */
public class ResultInfo {

    @Getter @Setter
    private String status_code;

    @Getter @Setter
    private String description;
}
