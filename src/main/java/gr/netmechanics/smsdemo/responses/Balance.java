package gr.netmechanics.smsdemo.responses;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Nikos Papadakis
 */
public class Balance {

    @Setter @Getter
    private String balance;

    @Setter @Getter
    private String reserved;

    @Setter @Getter
    private String plafon;

    @Setter @Getter
    private List<Subscriptions> subscriptions;
}
