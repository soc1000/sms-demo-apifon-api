package gr.netmechanics.smsdemo.responses;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Nikos Papadakis
 */
public class MessageStatus {

    @Getter @Setter
    private int code;

    @Getter @Setter
    private String text;
}
