package gr.netmechanics.smsdemo.responses;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Nikos Papadakis
 */
public class CallbackResponse {

    @Getter @Setter
    private String url;

    @Getter @Setter
    private String request_id;

    @Getter @Setter
    private String reference_id;

    @Getter @Setter
    private CallbackInfo data;

    @Getter @Setter
    private String account_id;

    @Getter @Setter
    private String type;
}
