package gr.netmechanics.smsdemo.responses;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Nikos Papadakis
 */
public class CallbackInfo {

    @Getter @Setter
    private String from;

    @Getter @Setter
    private String to;

    @Getter @Setter
    private String message_id;

    @Getter @Setter
    private String custom_id;

    @Getter @Setter
    private MessageStatus status;

    @Getter @Setter
    private String error_code;

    @Getter @Setter
    private Long timestamp;

    @Getter @Setter
    private String mccmnc;
}
